import boto3
from utilities import utils
import logging

def get_reservation_purchase_recommendation():
    """
    Gets recommendations for which reservations to purchase.
    These recommendations could help you reduce your costs.
    :return:
    """
    try:
        ce_client = utils.create_session('ce', "us-east-1")
        response = ce_client.get_reservation_purchase_recommendation(
            Service='ec2'
        )
        print(response)
    except Exception as e:
        logging.error(e)

if __name__ == "__main__":
    get_reservation_purchase_recommendation()


