sql_conn_cred = {
"host":"prodops-test-1.ctqdxnrech1e.us-east-1.rds.amazonaws.com",
"user":"admin",
"passwd":"SGb8uNyfHWOqkGsnrvGL",
"database":"aws"
}

names = {
    "excel_file_name": "Approved EC2 Instances",
    "db_name": "AWS"
}

db_name_sheet_name_mapping = {
    "Approved Instances": "approved_instances",
    "Approved RDS": "approved_rds",
    "Elasticache": "elasticache",
    "Redshift Instances": "redshift_instances"
}

sheet_to_primary_key_mapping = {
    "Approved Instances": ["instance_name", "project", "region","instancetype", "should_reserve"],
    "Approved RDS": ["name", "project", "region","should_reserve","multi_az","engine"],
    "Elasticache": ["name", "project", "region","instancetype","should_reserve","engine"],
    "Redshift Instances": ["name", "owner", "region","should_reserve"]
}

primary_key_to_actual_column_map = {
    "instance_name": "Instance Name",
    "project": "Project",
    "instancetype": "InstanceType",
    "name": "Name",
    "owner": "Owner",
    "should_reserve": "Should Reserve",
    "region":"Region",
    "multi_az": "multi-AZ",
    "engine":"Engine"
}


create_db = True
create_tables = True

google_cred_path = "credentials.json"