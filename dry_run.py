import pprint
import logging
from utilities import utils as util
from config import google_cred_path, sql_conn_cred
from config import db_name_sheet_name_mapping as db_name
import csv
import json


def dry_run():
    gspread_conn = util.create_gspread_conn(google_cred_path)

    db_conn = util.create_db_conn(sql_conn_cred)
    db_cursor = db_conn.cursor()


    spreadsheet_metadata = util.get_sheet_metadata(gspread_conn)

    sheet_names_list = util.get_sheet_names(spreadsheet_metadata)

    records_going_to_change = list()
    records_going_to_delete = list()
    existing_data = list()
    new_data = list()
    update_data= list()

    for sheet in sheet_names_list[:1]:
        column_names, spreadsheet_records = util.get_records(gspread_conn, sheet)
        columns_list = list()
        for row in spreadsheet_records:
            columns_list.append(tuple(map(str, row.values())))

        db_cursor.execute("SELECT instance_name, project, componentid, instancetype,\
                                comments, environment, region, terminate_eta,should_reserve,\
                                conditional_approval FROM {};"
                                          .format(db_name[sheet]))
        mysql_records = db_cursor.fetchall()

        for srow in columns_list:
            if srow in mysql_records:
                pass
            else:
                records_going_to_change.append(srow)

        for record_to_be_updated in records_going_to_change:
            db_cursor.execute("SELECT id FROM {} WHERE instance_name='{}'".format(
                db_name[sheet], record_to_be_updated[0]))
            id_detail = db_cursor.fetchall()
            if id_detail != []:
                id = str(id_detail[0]).split(',')[0][1:]
                db_cursor.execute("SELECT instance_name, project, componentid, instancetype,\
                                                comments, environment, region, terminate_eta,should_reserve,\
                                                conditional_approval FROM {} WHERE id={};"
                                  .format(db_name[sheet],id))
                existing_data.append(db_cursor.fetchall())
                update_data.append(record_to_be_updated)
            else:
                new_data.append(record_to_be_updated)
        for dbrow in mysql_records:
            if dbrow not in columns_list:
                records_going_to_delete.append(dbrow)

        with open("{}.csv".format(sheet),"w") as fp:
            writer = csv.writer(fp)
            if new_data != []:
                writer.writerow(["NEW DATA TO BE INSERTED"])
                writer.writerow(["instance_name", "project", "componentid", "instancetype",
                                                    "comments", "environment", "region", "terminate_eta,should_reserve",
                                                    "conditional_approval"])
                new_data = json.dumps(new_data)
                new_data = json.loads((new_data.replace("(","[")).replace(")","]"))
                writer.writerows(new_data)
            if records_going_to_delete != []:
                writer.writerow(["DATA GOING TO BE DELETED"])
                writer.writerow(["instance_name", "project", "componentid", "instancetype",
                                "comments", "environment", "region", "terminate_eta","should_reserve",
                                "conditional_approval"])
                records_going_to_delete = json.dumps(records_going_to_delete)
                records_going_to_delete = json.loads((records_going_to_delete.replace("(","[")).replace(")","]"))
                writer.writerows(records_going_to_delete)

            if existing_data:
                writer.writerow(["DATA GOING TO BE CHANGE"])
                writer.writerow(["instance_name", "project", "componentid", "instancetype",
                                "comments", "environment", "region", "terminate_eta","should_reserve",
                                "conditional_approval"])
                existing_data = json.dumps(existing_data)
                existing_data = json.loads((existing_data.replace("(", "[")).replace(")", "]"))
                writer.writerows(existing_data)
            if update_data != []:
                writer.writerow(["DATA AFTER DB UPDATE"])
                writer.writerow(["instance_name", "project", "componentid", "instancetype",
                                "comments", "environment", "region", "terminate_eta","should_reserve",
                                "conditional_approval"])
                update_data = json.dumps(update_data)
                update_data = json.loads((update_data.replace("(", "[")).replace(")", "]"))
                writer.writerows(update_data)


if __name__ == "__main__":
    dry_run()

