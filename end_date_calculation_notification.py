from config import sql_conn_cred
from utilities import utils as util
from datetime import datetime
import logging
import json



def get_due_instances():
    """
    This function will get the list of instances whose end date is over.
    :return:
    """
    try:

        db_conn = util.create_db_conn(sql_conn_cred)
        db_cursor = db_conn.cursor()

        current_date = datetime.date(datetime.now())

        db_cursor.execute("SELECT instance_name, terminate_eta from approved_instances WHERE terminate_eta != ''")
        db_data = db_cursor.fetchall()

        due_list = dict()
        due_list["OverDue"] = list()
        due_list["Expiring_today"] = list()
        due_list["about_to_expire"] = list()

        for data in db_data:
            eta_date = datetime.strptime(data[1], '%m/%d/%Y').date()
            delta = (eta_date - current_date).days
            print(delta)
            if delta < 0:
                #Send an email for the overdue laps
                print("OverDue")
                due_list["OverDue"].append(data[0])

            elif delta == 0:
                # last day notification
                print("Ending Today")
                due_list["Expiring_today"].append(data[0])
            elif 0 < delta <= 7:
                print("Warning Email")
                due_list["about_to_expire"].append(data[0])
        with open("instance_terminate_ETA.json","w") as fp:
            fp.write(json.dumps(due_list))
    except Exception as e:
        logging.error(e)

if __name__ == "__main__":
    get_due_instances()