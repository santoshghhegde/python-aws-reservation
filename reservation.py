import boto3
import json
import logging
import math
import pandas as pd
import pprint

from utilities import utils as util
from config import sql_conn_cred
from collections import defaultdict

pp = pprint.PrettyPrinter(indent=4)
nested_dict = lambda: defaultdict(nested_dict)

    
def ec2():
    reserved_instances_details = nested_dict()
    db_data = util.get_db_data("infraventory.bas")
    db_data_count = nested_dict()
    aws_count = {}

    for region, region_data in db_data["reserve"].items():
        for component_id, instance_type_data in region_data.items():
            for instance_type, count in instance_type_data.items():
                if instance_type in db_data_count[region]:
                    db_data_count[region][instance_type] += int(count)
                else:
                    db_data_count[region][instance_type] = int(count)

    diff_count = db_data_count

    for region in db_data["regions"]:
        ec2_client = util.create_session('ec2',region)
        instances = ec2_client.describe_reserved_instances(
                Filters=[
                        {
                            "Name": "state",
                            "Values": [
                                "active"
                            ]
                        }
                ]
        )["ReservedInstances"]

        for instance in instances:
            if instance["InstanceType"] in reserved_instances_details[region]:
                reserved_instances_details[region][instance["InstanceType"]] += int(instance["InstanceCount"])
            else:
                reserved_instances_details[region][instance["InstanceType"]] = int(instance["InstanceCount"])


    for region, instance_types in db_data_count.items():
        for instance_type, count in instance_types.items():
            try:
                if instance_type in reserved_instances_details[region].keys():
                    diff_count[region][instance_type] = db_data_count[region][instance_type] - reserved_instances_details[region][instance_type]
            except Exception as error:
                print(error)

    print(f'Region, InstanceType, Count')
    for region, instance_types in diff_count.items():
        for instance_type, count in instance_types.items():
            print(f'{region}, {instance_type}, {count}')

def rds_normalized():
    available_regions = boto3.Session().get_available_regions("rds")
    count=0
    overutilized = []
    underutilized = []
    aws_count = {}
    for region in available_regions:
       
        rds_client = util.create_session('rds', region_name=region)
        try:
            #response = rds_client.describe_db_instances()
            #print(rds_client.can_paginate("describe_db_instances"))
            paginator = rds_client.get_paginator("describe_reserved_db_instances")
            response = paginator.paginate().build_full_result()
            #print(response)
            instances_list = response["ReservedDBInstances"]
            for instance_index in range(len(instances_list)):
                if(response["ReservedDBInstances"][instance_index]["State"] == "active"):
                    
                    instance_type = response["ReservedDBInstances"][instance_index]["DBInstanceClass"]
                    db_count = response["ReservedDBInstances"][instance_index]["DBInstanceCount"]
                    multi_az = response["ReservedDBInstances"][instance_index]["MultiAZ"]
                    if(multi_az == True):
                        
                        multi_az = "y"
                    else:
                        multi_az = "n"
                    engine = response["ReservedDBInstances"][instance_index]["ProductDescription"]
                    
                    key_tuple = (instance_type,region,multi_az,engine)
                    
                    if (key_tuple in aws_count.keys()):
                        aws_count[key_tuple] += db_count
                    else:
                        aws_count[key_tuple] = db_count
                
            
        except:
            count+=1
            continue
    
    mysql_count = {}
    db_conn = util.create_db_conn(sql_conn_cred)
    db_cursor = db_conn.cursor()
    query_to_get_instance_types = "SELECT instancetype,region,no_of_instances,should_reserve,multi_az,engine from approved_rds;"
    db_cursor.execute(query_to_get_instance_types)
    mysql_data = db_cursor.fetchall()
    for instance_region_number_tuple in mysql_data:
        instance_region_tuple = (instance_region_number_tuple[0], instance_region_number_tuple[1], instance_region_number_tuple[4], instance_region_number_tuple[5].lower())
        #print(instance_region_tuple[-1])
        if(instance_region_number_tuple[3] == "Yes"):
            if (instance_region_tuple in mysql_count.keys()):
                mysql_count[instance_region_tuple] += int(instance_region_number_tuple[2])
            else:
                mysql_count[instance_region_tuple] = int(instance_region_number_tuple[2])
           
    #print(mysql_count)
   
    diff_count = {}
    for instance_region_tuple in mysql_count.keys():
        if (instance_region_tuple in aws_count.keys()):
            diff_count[instance_region_tuple] = mysql_count[instance_region_tuple] - aws_count[instance_region_tuple]
        else:
            diff_count[instance_region_tuple] = mysql_count[instance_region_tuple]
    

    #print that these instances need to be reserved
    
    for instance_region_tuple in diff_count.keys():
        if (diff_count[instance_region_tuple] > 0):
            #print("{} instances of type {} in region {} should be reserved".format(diff_count[instance_region_tuple], instance_region_tuple[0], instance_region_tuple[1]))
            instance_dict = {"Region": instance_region_tuple[1], "InstanceType": instance_region_tuple[0],"Multi-AZ": instance_region_tuple[2], "Engine": instance_region_tuple[3], "Number to reserve": diff_count[instance_region_tuple]}
            if(instance_dict not in underutilized):
                underutilized.append(instance_dict)
        elif (diff_count[instance_region_tuple] < 0):
            instance_dict = {"Region": instance_region_tuple[1], "InstanceType": instance_region_tuple[0], "Multi-AZ": instance_region_tuple[2], "Engine": instance_region_tuple[3],"Number overutilized": abs(diff_count[instance_region_tuple])}
            if(instance_dict not in overutilized):
                overutilized.append(instance_dict)
    
    for instance_region_tuple in aws_count.keys():
        if (instance_region_tuple not in mysql_count.keys()):
            instance_dict = {"Region": instance_region_tuple[1], "InstanceType": instance_region_tuple[0],"Multi-AZ": instance_region_tuple[2], "Engine": instance_region_tuple[3], "Number overutilized": aws_count[instance_region_tuple]}
            if(instance_dict not in overutilized):
                overutilized.append(instance_dict)
   
    overutilized_df = pd.DataFrame(overutilized)
    underutilized_df = pd.DataFrame(underutilized)

    underlist,overlist = [],[]

    if(len(overutilized) > 0):
        overutilized_df = overutilized_df.groupby(["Region", "InstanceType","Multi-AZ","Engine"]).agg({'Number overutilized': 'sum'})
        list_of_dictionaries = []
        for index in overutilized_df.index:
            new_dict = {}
            new_dict["Region"] = index[0]
            new_dict["InstanceType"] = index[1]
            new_dict["Multi-AZ"] = index[2]
            new_dict["Engine"] = index[3]
            new_dict["Number to reserve"] = -1* overutilized_df['Number overutilized'].loc[index]
            list_of_dictionaries.append(new_dict)
        overlist = list_of_dictionaries
        overutilized = pd.DataFrame(list_of_dictionaries)
    if(len(underutilized) > 0):
        underutilized_df = underutilized_df.groupby(["Region", "InstanceType","Multi-AZ","Engine"]).agg({"Number to reserve": "sum"})
        list_of_dictionaries = []
        for index in underutilized_df.index:
            new_dict = {}
            new_dict["Region"] = index[0]
            new_dict["InstanceType"] = index[1]
            new_dict["Multi-AZ"] = index[2]
            new_dict["Engine"] = index[3]
            new_dict["Number to reserve"] = underutilized_df['Number to reserve'].loc[index]
            list_of_dictionaries.append(new_dict)
        underlist = list_of_dictionaries
        underutilized = pd.DataFrame(list_of_dictionaries)

    combined_list = overlist + underlist
    combined_df = pd.DataFrame(combined_list)
    combined_df = combined_df.groupby(["Region", "InstanceType","Multi-AZ","Engine"]).agg({"Number to reserve": "sum"})
    list_of_dictionaries = []
    for index in combined_df.index:
        new_dict = {}
        new_dict["Region"] = index[0]
        new_dict["InstanceType"] = index[1]
        new_dict["Multi-AZ"] = index[2]
        new_dict["Engine"] = index[3]
        new_dict["Number to reserve"] = combined_df['Number to reserve'].loc[index]
        list_of_dictionaries.append(new_dict)
    combined = pd.DataFrame(list_of_dictionaries)
    return combined



def redshift_normalized():
    available_regions = boto3.Session().get_available_regions("redshift")
#    print(available_regions)
    overutilized = []
    underutilized = []
    aws_count = {}
    for region in available_regions:
        try:
            redshift_client = util.create_session('redshift', region_name=region)
            paginator = redshift_client.get_paginator("describe_reserved_nodes")     
            data = paginator.paginate().build_full_result()
            
            #data = redshift_client.describe_reserved_nodes()   
            for reserved_node_index in range(len(data["ReservedNodes"])):
                node_type = data["ReservedNodes"][reserved_node_index]["NodeType"]
                number_of_nodes = data["ReservedNodes"][reserved_node_index]["NodeCount"]
                if(data["ReservedNodes"][reserved_node_index]["State"] == "active"):
                    instance_region_tuple = (node_type, region)
                    if (instance_region_tuple in aws_count.keys()):
                        aws_count[instance_region_tuple] += number_of_nodes
                    else:
                        aws_count[instance_region_tuple] = number_of_nodes
        except Exception as e:
            continue
    
    mysql_count = {}
    db_conn = util.create_db_conn(sql_conn_cred)
    db_cursor = db_conn.cursor()
    query_to_get_instance_types = "SELECT node_type, region, no_of_instances, should_reserve from redshift_instances;"
    db_cursor.execute(query_to_get_instance_types)
    mysql_data = db_cursor.fetchall()
    for instance_region_number_tuple in mysql_data:
        if(instance_region_number_tuple[3] == "yes"):
            instance_region_tuple = (instance_region_number_tuple[0], instance_region_number_tuple[1])
            if (instance_region_tuple in mysql_count.keys()):
                mysql_count[instance_region_tuple] += int(instance_region_number_tuple[2])
            else:
                mysql_count[instance_region_tuple] = int(instance_region_number_tuple[2])
    
    diff_count = {}
    for instance_region_tuple in mysql_count.keys():
        if (instance_region_tuple in aws_count.keys()):
            diff_count[instance_region_tuple] = mysql_count[instance_region_tuple] - aws_count[instance_region_tuple]
        else:
            diff_count[instance_region_tuple] = mysql_count[instance_region_tuple]
    

    #print that these instances need to be reserved
    
    for instance_region_tuple in diff_count.keys():
        if (diff_count[instance_region_tuple] > 0):
            #print("{} instances of type {} in region {} should be reserved".format(diff_count[instance_region_tuple], instance_region_tuple[0], instance_region_tuple[1]))
            instance_dict = {"Region": instance_region_tuple[1], "NodeType": instance_region_tuple[0], "Number to reserve": diff_count[instance_region_tuple]}
            if(instance_dict not in underutilized):
                underutilized.append(instance_dict)
        elif (diff_count[instance_region_tuple] < 0):
            instance_dict = {"Region": instance_region_tuple[1], "NodeType": instance_region_tuple[0], "Number overutilized": abs(diff_count[instance_region_tuple])}
            if(instance_dict not in overutilized):
                overutilized.append(instance_dict)
    
    for instance_region_tuple in aws_count.keys():
        if (instance_region_tuple not in mysql_count.keys()):
            instance_dict = {"Region": instance_region_tuple[1], "NodeType": instance_region_tuple[0], "Number overutilized": aws_count[instance_region_tuple]}
            if(instance_dict not in overutilized):
                overutilized.append(instance_dict)
   
    overutilized_df = pd.DataFrame(overutilized)
    underutilized_df = pd.DataFrame(underutilized)

    overlist, underlist = [], []
    if(len(overutilized) > 0):
        overutilized_df = overutilized_df.groupby(["Region", "NodeType"]).agg({'Number overutilized': 'sum'})
        list_of_dictionaries = []
        for index in overutilized_df.index:
            new_dict = {}
            new_dict["Region"] = index[0]
            new_dict["NodeType"] = index[1]
            new_dict["Number to reserve"] = -1*overutilized_df['Number overutilized'].loc[index]
            list_of_dictionaries.append(new_dict)
        overlist = list_of_dictionaries
        overutilized = pd.DataFrame(list_of_dictionaries)
    if(len(underutilized) > 0):
        underutilized_df = underutilized_df.groupby(["Region", "NodeType"]).agg({"Number to reserve": "sum"})
        list_of_dictionaries = []
        for index in underutilized_df.index:
            new_dict = {}
            new_dict["Region"] = index[0]
            new_dict["NodeType"] = index[1]
            new_dict["Number to reserve"] = underutilized_df['Number to reserve'].loc[index]
            list_of_dictionaries.append(new_dict)
        underlist = list_of_dictionaries
        underutilized = pd.DataFrame(list_of_dictionaries)
    combined_list = overlist + underlist
    combined_df = pd.DataFrame(combined_list)
    combined_df = combined_df.groupby(["Region", "NodeType"]).agg({"Number to reserve": "sum"})
    list_of_dictionaries = []
    for index in combined_df.index:
        new_dict = {}
        new_dict["Region"] = index[0]
        new_dict["NodeType"] = index[1]
        new_dict["Number to reserve"] = combined_df['Number to reserve'].loc[index]
        list_of_dictionaries.append(new_dict)
    combined = pd.DataFrame(list_of_dictionaries)
    return combined


def elasticache_normalize():
    available_regions = boto3.Session().get_available_regions("elasticache")
    overutilized = []
    underutilized = []
    aws_count = {}
    for region in available_regions:
        try:
            ec_client = util.create_session('elasticache', region_name=region)
            paginator = ec_client.get_paginator("describe_reserved_cache_nodes")     
            data = paginator.paginate().build_full_result()
            for reserved_node_index in range(len(data["ReservedCacheNodes"])):
                if(data["ReservedCacheNodes"][reserved_node_index]["State"] == "active"):
                    
                    node_type = data["ReservedCacheNodes"][reserved_node_index]["CacheNodeType"]
                    number_of_nodes = data["ReservedCacheNodes"][reserved_node_index]["CacheNodeCount"]
                    engine = data["ReservedCacheNodes"][reserved_node_index]["ProductDescription"]
                    instance_region_tuple = (node_type, region, engine)
                    if (instance_region_tuple in aws_count.keys()):
                        aws_count[instance_region_tuple] += number_of_nodes
                    else:
                        aws_count[instance_region_tuple] = number_of_nodes
        except Exception as e:
            
            continue
    mysql_count = {}
    db_conn = util.create_db_conn(sql_conn_cred)
    db_cursor = db_conn.cursor()
    query_to_get_instance_types = "SELECT instancetype, region, no_of_instances, should_reserve, engine from elasticache;"
    db_cursor.execute(query_to_get_instance_types)
    mysql_data = db_cursor.fetchall()
    for instance_region_number_tuple in mysql_data:
        if(instance_region_number_tuple[3] == "Yes"):
            instance_region_tuple = (instance_region_number_tuple[0], instance_region_number_tuple[1], instance_region_number_tuple[4])
            
            if (instance_region_tuple in mysql_count.keys()):
                mysql_count[instance_region_tuple] += int(instance_region_number_tuple[2])
            else:
                mysql_count[instance_region_tuple] = int(instance_region_number_tuple[2])
    diff_count = {}
    for instance_region_tuple in mysql_count.keys():
        if (instance_region_tuple in aws_count.keys()):
            diff_count[instance_region_tuple] = mysql_count[instance_region_tuple] - aws_count[instance_region_tuple]
        else:
            diff_count[instance_region_tuple] = mysql_count[instance_region_tuple]
    

    #print that these instances need to be reserved
    
    for instance_region_tuple in diff_count.keys():
        if (diff_count[instance_region_tuple] > 0):
            #print("{} instances of type {} in region {} should be reserved".format(diff_count[instance_region_tuple], instance_region_tuple[0], instance_region_tuple[1]))
            instance_dict = {"Region": instance_region_tuple[1], "NodeType": instance_region_tuple[0], "Engine": instance_region_tuple[2],"Number to reserve": diff_count[instance_region_tuple]}
            if(instance_dict not in underutilized):
                underutilized.append(instance_dict)
        elif (diff_count[instance_region_tuple] < 0):
            instance_dict = {"Region": instance_region_tuple[1], "NodeType": instance_region_tuple[0], "Engine": instance_region_tuple[2],"Number overutilized": abs(diff_count[instance_region_tuple])}
            if(instance_dict not in overutilized):
                overutilized.append(instance_dict)
    
    for instance_region_tuple in aws_count.keys():
        if (instance_region_tuple not in mysql_count.keys()):
            instance_dict = {"Region": instance_region_tuple[1], "NodeType": instance_region_tuple[0], "Engine": instance_region_tuple[2],"Number overutilized": aws_count[instance_region_tuple]}
            if(instance_dict not in overutilized):
                overutilized.append(instance_dict)
   
    overutilized_df = pd.DataFrame(overutilized)
    underutilized_df = pd.DataFrame(underutilized)
    underlist, overlist = [],[]
    if(len(overutilized) > 0):
        overutilized_df = overutilized_df.groupby(["Region", "NodeType", "Engine"]).agg({'Number overutilized': 'sum'})
        list_of_dictionaries = []
        for index in overutilized_df.index:
            new_dict = {}
            new_dict["Region"] = index[0]
            new_dict["NodeType"] = index[1]
            new_dict["Engine"] = index[2]
            new_dict["Number to reserve"] = -1*overutilized_df['Number overutilized'].loc[index]
            list_of_dictionaries.append(new_dict)
        overlist = list_of_dictionaries
        overutilized = pd.DataFrame(list_of_dictionaries)
    if(len(underutilized) > 0):
        underutilized_df = underutilized_df.groupby(["Region", "NodeType","Engine"]).agg({"Number to reserve": "sum"})
        list_of_dictionaries = []
        for index in underutilized_df.index:
            new_dict = {}
            new_dict["Region"] = index[0]
            new_dict["NodeType"] = index[1]
            new_dict["Engine"] = index[2]
            new_dict["Number to reserve"] = underutilized_df['Number to reserve'].loc[index]
            list_of_dictionaries.append(new_dict)
        underlist = list_of_dictionaries
        underutilized = pd.DataFrame(list_of_dictionaries)
    combined_list = overlist + underlist
    combined_df = pd.DataFrame(combined_list)
    combined_df = combined_df.groupby(["Region", "NodeType", "Engine"]).agg({"Number to reserve": "sum"})
    list_of_dictionaries = []
    for index in combined_df.index:
        new_dict = {}
        new_dict["Region"] = index[0]
        new_dict["NodeType"] = index[1]
        new_dict["Engine"] = index[2]
        new_dict["Number to reserve"] = combined_df['Number to reserve'].loc[index]
        list_of_dictionaries.append(new_dict)
    combined = pd.DataFrame(list_of_dictionaries)
    return combined
    