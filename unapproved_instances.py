import pprint
import logging
from utilities import utils as util
import os
import boto3
from config import db_name_sheet_name_mapping as db_name

from config import google_cred_path, sql_conn_cred
from dateutil.tz import tzutc

import json
import sys
import datetime
from collections import defaultdict

pp = pprint.PrettyPrinter(indent=4)

nested_dict = lambda: defaultdict(nested_dict)
aws_data_ondemand = defaultdict(dict)
aws_data_spot = defaultdict(dict)

final_data = nested_dict()

def get_db_data():
    db_data = nested_dict()
#    db_data["count_component_id"] = {}
#    db_data["reserve"] = {}
#    db_data["dont_reserve"] = {}
    db_conn = util.create_db_conn(sql_conn_cred)
    db_cursor = db_conn.cursor()
    db_cursor.execute("SELECT * FROM infraventory.bas")
    all_data = db_cursor.fetchall()
#    print(all_data)

    regions = [region[6] for region in all_data]
    regions = set(regions)

    db_data["regions"] = [region for region in regions]

    for data in all_data:
        if data[8]:
            if data[3] in db_data[data[6]]["reserve"][data[2]]:
                db_data[data[6]]["reserve"][data[2]][data[3]] += int(data[9])
            else:
                db_data[data[6]]["reserve"][data[2]][data[3]] = int(data[9])
        else:
            if data[3] in db_data[data[6]]["dont_reserve"][data[2]]:
                    db_data[data[6]]["dont_reserve"][data[2]][data[3]] += int(data[9])
            else:
                db_data[data[6]]["dont_reserve"][data[2]][data[3]] = int(data[9])
    return db_data

def get_od_aws_data(**kwargs):
    data = nested_dict()
    filtered_describe_instances = {}
    current_time = datetime.datetime.now(tz=tzutc())

    client = boto3.client('ec2', region_name=kwargs['region'])
    paginator = client.get_paginator("describe_instances")
    describe_instances = paginator.paginate(
                Filters=[
                            {
                                'Name': 'instance-state-name',
                                'Values': [
                                    'running',
                                ]
                            }
                        ]
                    )

    for all_data in describe_instances:
        for instance_data in all_data["Reservations"]:
            if not "InstanceLifecycle" in instance_data["Instances"][0].keys():
                for instances in instance_data["Instances"]:
                    tag_project = [tag for tag in instances["Tags"] if tag["Key"].lower() == "project"]
                    tag_name = [tag for tag in instances["Tags"] if tag["Key"] == "Name"]
                    tag_component_id = [tag for tag in instances["Tags"] if tag["Key"] == "ComponentId"]

                    try:
                        tag_component_id[0]["Value"] = int(tag_component_id[0]["Value"])
                    except Exception as e:
                        logging.info(e)

                    if tag_component_id and isinstance(tag_component_id[0]["Value"], int):
#                        print(tag_component_id[0]["Value"])
                        data["with_component_id"][int(tag_component_id[0]["Value"])][instances["InstanceId"]]["LaunchTime"] = int((current_time - instances["LaunchTime"]).days)
                        data["with_component_id"][int(tag_component_id[0]["Value"])][instances["InstanceId"]]["InstanceType"] = instances["InstanceType"]

                        if tag_name:
                            data["with_component_id"][int(tag_component_id[0]["Value"])][instances["InstanceId"]]["Name"] = tag_name[0]["Value"]
                        else:
                            data["with_component_id"][int(tag_component_id[0]["Value"])][instances["InstanceId"]]["Name"] = "None"
                        if tag_project:
                            data["with_component_id"][int(tag_component_id[0]["Value"])][instances["InstanceId"]]["Project"] = tag_project[0]["Value"]
                        else:
                            data["with_component_id"][int(tag_component_id[0]["Value"])][instances["InstanceId"]]["Project"] = "None"
        
                        if tag_component_id[0]["Value"] in data["count_component_id"]:
                            if instances["InstanceType"] in data["count_component_id"][int(tag_component_id[0]["Value"])]:
                                data["count_component_id"][int(tag_component_id[0]["Value"])][instances["InstanceType"]] += 1
                            else:
                                data["count_component_id"][int(tag_component_id[0]["Value"])][instances["InstanceType"]] = 1
                        else:
                            data["count_component_id"][int(tag_component_id[0]["Value"])][instances["InstanceType"]] = 1 

                    else:
                        tag_component_id = [{"Value" : 999999}]
                        data["without_component_id"][int(tag_component_id[0]["Value"])][instances["InstanceId"]]["LaunchTime"] = int((current_time - instances["LaunchTime"]).days)
                        data["without_component_id"][int(tag_component_id[0]["Value"])][instances["InstanceId"]]["InstanceType"] = instances["InstanceType"]

                        if tag_name:
                            data["without_component_id"][int(tag_component_id[0]["Value"])][instances["InstanceId"]]["Name"] = tag_name[0]["Value"]
                        else:
                            data["without_component_id"][int(tag_component_id[0]["Value"])][instances["InstanceId"]]["Name"] = "None"
                            
                        if tag_project:
                            data["without_component_id"][int(tag_component_id[0]["Value"])][instances["InstanceId"]]["Project"] = tag_project[0]["Value"]
                        else:
                            data["without_component_id"][int(tag_component_id[0]["Value"])][instances["InstanceId"]]["Project"] = "None"
    return data
#    print(data)


def get_unapproved_instances_list():
    db_data = util.get_db_data("infraventory.bas")
#    pp.pprint(db_data)

    regions = [region[6] for region in db_data]
    regions = set(regions)
#    db_data["regions"] = ["us-west-2"]
    for region in db_data["regions"]:
#        print(region)
        aws_data_ondemand[region] = get_od_aws_data(region=region)
#    pp.pprint(aws_data_ondemand)

    diff_db = db_data
    diff_aws = aws_data_ondemand

    for region, cid_data in db_data["reserve"].items():
        for cid, itypes in cid_data.items():
            for itype, count in itypes.items():
                if region in aws_data_ondemand:
                    if cid in aws_data_ondemand[region]["count_component_id"]:
                        if itype in aws_data_ondemand[region]["count_component_id"][cid]:
                            temp_count = int(db_data["reserve"][region][cid][itype]) - int(aws_data_ondemand[region]["count_component_id"][cid][itype])
                            if temp_count < 0:
                                final_data["Unapproved"][region][cid][itype] = abs(temp_count)                            

                            elif temp_count > 0:
                                final_data["ApprovedButNotUsed"][region][cid][itype] = abs(temp_count)
                        else:
                            final_data["ApprovedButNotUsed"][region][cid][itype] = int(db_data["reserve"][region][cid][itype])
                    elif cid:
                        final_data["ApprovedButNotUsed"][region][cid][itype] = int(db_data["reserve"][region][cid][itype])
                    else:
                        final_data["CIDNotExistInDB"][region][cid][itype] = int(db_data["reserve"][region][cid][itype])
                else:
                    final_data["ApprovedButNotUsed"][region][cid][itype] = int(db_data["reserve"][region][cid][itype])

    print(f"Status, Region, ComponentId, InstanceType, count")
    for status, regions in  final_data.items():
        for region, cids in regions.items():
            for cid, itypes in cids.items():
                for itype, count in itypes.items():
                    if count:
                        print(f"{status}, {region}, {cid}, {itype}, {count}")

    print(f"\nStatus, Region, Project, Name, InstanceType, RunningSince")
    for region, all_data in aws_data_ondemand.items():
        for cid, instanceids in all_data["without_component_id"].items():
            for instanceid, itypes in instanceids.items():
                print(f"NotTagged, {region},{itypes['Project']}, {itypes['Name']}, {itypes['InstanceType']}, {itypes['LaunchTime']} days")










#    pp.pprint(util.default_to_regular(final_data))
