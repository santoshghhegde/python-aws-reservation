import pprint

from ast import literal_eval
from config import google_cred_path, sql_conn_cred
from config import db_name_sheet_name_mapping as db_name
from config import primary_key_to_actual_column_map as pk_to_column
from config import sheet_to_primary_key_mapping as sheet_to_pk
from read_gspread import does_everything_match
from read_gspread import get_columns_list
from utilities import utils as util


def update_db():
    gspread_conn = util.create_gspread_conn(google_cred_path)

    db_conn = util.create_db_conn(sql_conn_cred)
    db_cursor = db_conn.cursor()

    spreadsheet_metadata = util.get_sheet_metadata(gspread_conn)

    sheet_names_list = util.get_sheet_names(spreadsheet_metadata)
    
    for sheet in sheet_names_list:
        if sheet in db_name.keys():
            column_names, spreadsheet_records = util.get_records(gspread_conn, sheet)
        
            table_column_names = list()
            for i in column_names:
                table_column_names.append(i.replace("-", "_").replace(" ", "_").lower())
            
            indices_of_pk = []
            for col_index in range(len(table_column_names)):
                col_name = table_column_names[col_index]
                if col_name in sheet_to_pk[sheet]:
                    indices_of_pk.append(col_index)
            
            sql_columns = ",".join(table_column_names)
            
            db_cursor.execute("SELECT * FROM {};"
                                           .format(db_name[sheet]))
            mysql_records = db_cursor.fetchall()
            

           
        
            columns_list = get_columns_list(sheet, column_names, spreadsheet_records)
            records_to_be_updated = list()
            records_to_be_insert = list()

            for row in columns_list:
                nrow = literal_eval(row) 
                if (nrow not in mysql_records):
                    records_to_be_updated.append(nrow)
            
            for record_to_be_updated in records_to_be_updated:
                id_of_record = []
                for index in indices_of_pk:
                    id_of_record.append(record_to_be_updated[index])
                
                primary_keys = sheet_to_pk[sheet]
                condition_string = ''
                if len(primary_keys) > 0:
                    condition_string += primary_keys[0] + (" = '{}'".format(id_of_record[0]))
                for i in range(1, len(primary_keys)):
                    string = " AND " + primary_keys[i] + (" = '{}'".format(id_of_record[i]))
                    condition_string += string
                db_cursor.execute("SELECT {} FROM {} WHERE {}".format(", ".join(primary_keys),db_name[sheet], condition_string))
                id_details = db_cursor.fetchall()
                if len(id_details) > 0:
                    print("updating", id_of_record)
                    sql_command = "UPDATE {} SET {} WHERE {}".format(db_name[sheet],"=%s, ".join(table_column_names)+"=%s",condition_string)
                    db_cursor.execute(sql_command,record_to_be_updated)
                    db_conn.commit()
                else:
                    print("inserting", id_of_record)
                    records_to_be_insert = [str(i) for i in record_to_be_updated]
                   
                    sql_insert_statement = "INSERT INTO {} ({}) VALUES {};".format(db_name[sheet],
                                        ",".join(table_column_names), tuple(records_to_be_insert))
                  
                    db_cursor.execute(sql_insert_statement)
                    db_conn.commit()
               
           
           
            records_to_be_deleted = list()
            db_cursor.execute("SELECT * FROM {};"
                                           .format(db_name[sheet]))
            mysql_records = db_cursor.fetchall()

            for row in mysql_records:
                
                if str(row) not in columns_list:
                    records_to_be_deleted.append(row)
            for record_to_be_deleted in records_to_be_deleted:
                id_of_record = []
                for index in indices_of_pk:
                    id_of_record.append(record_to_be_deleted[index])
                
                primary_keys = sheet_to_pk[sheet]
                condition_string = ''
                if len(primary_keys) > 0:
                    condition_string += primary_keys[0] + (" = '{}'".format(id_of_record[0]))
                for i in range(1, len(primary_keys)):
                    string = " AND " + primary_keys[i] + (" = '{}'".format(id_of_record[i]))
                    condition_string += string
                db_cursor.execute("DELETE from {} WHERE {}".format(db_name[sheet], condition_string))
                db_conn.commit()



if __name__ == "__main__":
    update_db()

