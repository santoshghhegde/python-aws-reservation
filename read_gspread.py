import pprint
from utilities import utils as util
from config import google_cred_path, sql_conn_cred
from config import db_name_sheet_name_mapping as db_name
from config import sheet_to_primary_key_mapping as primary_key
from config import primary_key_to_actual_column_map as pk_to_column


global list_of_sheets
pp = pprint.PrettyPrinter(indent=4)

def equal_dicts(d1, d2, ignore_keys):
    """
        Compares two dictionaries d1 and d2 while ignoring "ignore_keys" in making
        comparision.
    """
    d1_filtered = dict((k, v) for k,v in d1.items() if k not in ignore_keys)
    d2_filtered = dict((k, v) for k,v in d2.items() if k not in ignore_keys)
    #print (d1_filtered)
    return d1_filtered == d2_filtered

def does_everything_match(sheet,sheet_schema,all_rows_data, primary_key_values):
    """ 
    This function checks all the rows in the spreadsheet and determines the authenticity of 
    the primary key values.
    If all the columns values for all rows with same primary key match, then no of instances is updated to the
    sum of instances in each row.

    A boolean value and a single tuple with the updated number of instances is returned
    """
    base_row = {}
    count = 0
    total_instances = 0
    ignore_keys = ["Instance Name",'Name','No Of instances',"ComponentId",'Comments','Environment','Terminate ETA','Conditional Approval','Yearly On-Demand Cost','Yearly Cost of Reserving']
   
    for row in all_rows_data:
       
        primary_key_of_this_row = []
       
        for key in primary_key[sheet]:
            primary_key_of_this_row.append(row[pk_to_column[key]])
        if primary_key_of_this_row == primary_key_values:
            count += 1
            if (count == 1):
                base_row = row
            if (equal_dicts(row,base_row,ignore_keys) == False):
                return False, []
            else:
                total_instances += int(row['No Of instances'])

    base_row['No Of instances'] = str(total_instances)
    modified_row = []
    for column_name in sheet_schema:
        modified_row.append(str(base_row[column_name]))
    modified_row_string = str(tuple(modified_row))
    return True, modified_row_string



def get_columns_list(sheet, column_names, all_rows_data):
    """
        Returns all the rows (with their cumulative instances) to be inserted into the db.
    """
    columns_list = list()
    
    visited = []

    for row in all_rows_data:
    
        primary_key_values = []
        for key in primary_key[sheet]:
            if pk_to_column[key] not in row.keys():
                raise ValueError("primary key column " + pk_to_column[key] + " not present")
            
            primary_key_values.append(row[pk_to_column[key]])
        if primary_key_values not in visited:
    
            all_match, modified_row = does_everything_match(sheet, column_names, all_rows_data, primary_key_values)
            if all_match:
                columns_list.append(modified_row)
                visited.append(primary_key_values)
                
                
            else:
                raise ValueError("All rows having primary key as {} don't have matching values in other fields".format(primary_key_values))
    return columns_list

def create_table_and_populate_data():
    """
    This function creates tables in MySQL database and populates data to MYSQL from
    google spreadsheet
    :return:
    """
    try:

        util.create_database()

        gspread_conn = util.create_gspread_conn(google_cred_path)

        db_conn = util.create_db_conn(sql_conn_cred)
        db_cursor = db_conn.cursor()


        spreadsheet_metadata = util.get_sheet_metadata(gspread_conn)


        sheet_names_list = util.get_sheet_names(spreadsheet_metadata)

        for sheet in sheet_names_list:
            if sheet in db_name.keys():
                column_names, all_rows_data = util.get_records(gspread_conn, sheet)

                # empty space, - etc are replaced by _ in column names in order to use the same as
                # column names in tables

                table_column_names = list()
                for i in column_names:
                    table_column_names.append(i.replace("-", "_").replace(" ", "_").lower())
                sql_columns = ",".join(table_column_names)

                create_table_query = "CREATE TABLE {} (".format(db_name[sheet])
                for i in table_column_names:
                    create_table_query += "{} VARCHAR(500),".format(i.replace("-","_").replace(" ","_").
                                                            lower())
                
                
                create_table_query += "PRIMARY KEY ({}));".format(', '.join(primary_key[sheet]))

                db_cursor.execute(create_table_query)
                db_conn.commit()

                sql_insert_statement = "INSERT INTO {} ({}) VALUES {};"
                columns_list =  get_columns_list(sheet, column_names,all_rows_data)
                
                sql_insert_statement = sql_insert_statement.format(db_name[sheet], sql_columns, ",".join(columns_list))

                db_cursor.execute(sql_insert_statement)
                db_conn.commit()

    except Exception as e:
        pass

if __name__ == "__main__":
    create_table_and_populate_data()
