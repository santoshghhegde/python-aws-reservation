import requests
import json
from utilities import utils


def get_base():
    with open("normalization_config.json","r") as files:
        config = json.load(files)

    ec2_client = utils.create_session('ec2')
    #get count of (instancetype, region) pair in aws reserved instances
    response = ec2_client.describe_regions()
    regions = []
    for region in response["Regions"]:
        regions.append(region["RegionName"])
    finalMap = {}
    for region in regions:
        resp = requests.get("https://pricing.us-east-1.amazonaws.com/offers/v1.0/aws/AmazonEC2/current/" + region +"/index.json")
        data = json.loads(resp.content)
        instanceTypeList = {}
        for key in data["products"].keys():
            
            instance = data["products"][key]["attributes"]
            if("instanceType" in instance.keys()):
                instance = instance["instanceType"]
                attribute = instance.split(".")[-1]
                instanceType = instance.split(".")[0]
                if(instanceType in instanceTypeList.keys()):
                    if(attribute in config.keys()):
                        if(instanceTypeList[instanceType][0] > config[attribute]):
                            
                            instanceTypeList[instanceType]  = (config[attribute],attribute)
                else:
                    if(attribute in config.keys()):
                        instanceTypeList[instanceType] = (config[attribute], attribute)
        
    
        finalMap[region] = instanceTypeList

    with open("base_values.json","w") as f:
        json.dump(finalMap, f,indent=4) 
