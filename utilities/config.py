import json
import os


class Conf:
    def __init__(self):
        dir_name = os.path.dirname(__file__)
        conf_data = open(dir_name+"/conf.json", mode='r').read()
        conf = json.loads(conf_data)
        self.__dict__.update(conf)

