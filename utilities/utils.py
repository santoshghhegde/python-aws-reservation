import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pprint
import mysql.connector
import logging
import boto3
from config import names, sql_conn_cred
from collections import defaultdict

global list_of_sheets

pp = pprint.PrettyPrinter(indent=4)
nested_dict = lambda: defaultdict(nested_dict)

def default_to_regular(dict):
    if isinstance(dict, defaultdict):
        dict = {k: default_to_regular(v) for k, v in dict.items()}
    return dict

def create_gspread_conn(google_cred_path):
    """
    This function establishes connection with google spreadsheet via API and
    returns conn client.
    metadata
    :return:
    """

    try:
        scope = ['https://www.googleapis.com/auth/drive', 'https://spreadsheets.google.com/feeds']
        credentials = ServiceAccountCredentials.from_json_keyfile_name(google_cred_path, scope)
        logging.info("Found the credential in the environment")
        client = gspread.authorize(credentials)
        logging.debug("Client Connection created")

        return client

    except Exception as Error:
        logging.error(f"While creating a client connection the following error occurred - {Error}")

def get_sheet_metadata(client):
    """
    This function will return the spreadsheet metadata
    :param client:
    :return:
    """
    sheet_metadata = client.open(names['excel_file_name']).fetch_sheet_metadata()

    return sheet_metadata

def get_sheet_names(sheet_metadata):
    """
    This function extracts sheet names from google spreadsheet and
    return title of differenet sheets
    :param sheet_metadata:
    :return:
    """
    global list_of_sheets
    list_of_sheets = list()

    for sheet in sheet_metadata['sheets']:
        list_of_sheets.append(sheet['properties']['title'])

    return list_of_sheets


def get_records(client, sheet_name):
    """
    This function returns all the records for a particular sheet in the order Columns and
    rest of the records
    :param client:
    :return:
    """


    records = eval("client.open(names['excel_file_name']).\
                                worksheet('{}').get_all_records()".format(sheet_name))
    columns = eval("client.open(names['excel_file_name']).\
                                worksheet('{}').row_values(1)".format(sheet_name))

    return columns,records

def create_db_conn(sql_conn_cred):
    try:
        db_conn = mysql.connector.connect(**sql_conn_cred)
        return db_conn
    except Exception as Error:
        logging.error(f"Failed to create a database connection due to following error - {Error}")

def create_session(resource, region_name="us-east-1"):
    try:
        session = boto3.session.Session()
        client = session.client(resource, region_name)
        return client
    except Exception as Error:
        logging.error(f"Failed to create a database connection due to following error - {Error}")

def get_all_values_column(client, sheet_name, column_index):
    try:
        column_values = eval("client.open(names['excel_file_name']).\
                                        worksheet('{}').col_values({})".format(sheet_name, column_index))

        return column_values
    except Exception as Error:
        logging.error(f"Failed to create a database connection due to following error - {Error}")



def create_database():
    mydb = mysql.connector.connect(
        host=sql_conn_cred["host"],
        user=sql_conn_cred["user"],
        passwd=sql_conn_cred["passwd"]
    )
    mycursor = mydb.cursor()
    mycursor.execute("CREATE DATABASE IF NOT EXISTS {}".format(sql_conn_cred["database"]))

def get_db_data(table):
    db_data = nested_dict()
    db_conn = create_db_conn(sql_conn_cred)
    db_cursor = db_conn.cursor()
    db_cursor.execute(f'SELECT * FROM {table}')
    all_data = db_cursor.fetchall()

    regions = [region[6] for region in all_data]
    regions = set(regions)

    db_data["regions"] = [region for region in regions]
    for data in all_data:
        if data[8]:
            if data[3] in db_data["reserve"][data[6]][data[2]]:
                db_data["reserve"][data[6]][data[2]][data[3]] += int(data[9])
            else:
                db_data["reserve"][data[6]][data[2]][data[3]] = int(data[9])
        else:
            if data[3] in db_data["dont_reserve"][data[6]][data[2]]:
                    db_data["dont_reserve"][data[6]][data[2]][data[3]] += int(data[9])
            else:
                db_data["dont_reserve"][data[6]][data[2]][data[3]] = int(data[9])
    return default_to_regular(db_data)


def get_html_message():
    htres="<html><head><style>table, td, th {border: 3px solid silver;border-collapse: collapse;padding : " \
          "1px;text-align: center;}th {font-size:15px;}.notice{color:red}</style></head><p>This is an automated email"\
          " listing all the over provisioned ec2 instances. Request respective teams to action on it immediately. " \
          "The instance belong to below projects: </br></br>"+',\n '.join(final.keys())+"</p></br><table><tr><th>Name</th>" \
          "<th>ID</th><th>Availability Zone</th><th>Instance Type</th><th class='notice'>AVG CPU- Last 14 Days</th><" \
          "th>Project</th></tr>"

    for key,data in final.items():
        for aa in data:
            htres = htres + "<tr><td>" + str(aa[0]) + "</td><td>" + str(aa[1]) + "</td><td>" + str(
                aa[2]) + "</td><td>" + str(aa[3]) + "</td><td class='notice'>" + str(aa[4]) + "</td><td>" + str(
                aa[5]) + "</td></tr>"    
    htres=htres+"</table></html>"
    return htres


def send_email(htres):
    SMTP_SERVER = "smtprelay.bloomreach.com"
    SMTP_PORT = 587
    SMTP_USERNAME = "brsmtp@brcdn.net"
    SMTP_PASSWORD = "BloomReach4SMTP"

    EMAIL_TO = ["pratik.kumar@bloomreach.com"]
    EMAIL_FROM = "br-aws-reports@bloomreach.com"

    msg = MIMEMultipart('alternative')
    msg['Subject'] = "AWS Over Provisioned Resources - EC2"
    htmsg = MIMEText(htres, 'html')
    msg.attach(htmsg)

    mail = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    mail.ehlo()
    mail.ehlo()
    mail.login(SMTP_USERNAME, SMTP_PASSWORD)
    mail.sendmail(EMAIL_FROM, EMAIL_TO, msg.as_string())
    mail.quit()