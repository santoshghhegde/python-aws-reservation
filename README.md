# python-aws-reservation

This project is about maintaining records for unapproved instances and
normalization of instances through python scripts.

#### Prerequisites

- **Packages**  
        - boto3  
        - oauth2client  
        - gspread  
        - mysql.connector
- **AWS Credentials**  
        - AWS credentials i.e secret key and access key
        needs to be stored in .aws file
- **Python3.6** is required

#### Installing package

Use pip install <package_name>

#### Code Walkthrough  
##### main.py

- Start execution of any of the requirements from main.py file.
- Arguments used  
       - read_populate : to read data from google spreadsheet and 
       populate it to mysql db  
       - update_db : to update db  
       - unapproved_list : to get list of unapproved instances  
       - ec2_reserve : to get normalization factor  
       - overdue_instances : to get list of instance whose ETA is overdue or nearby  
       - vv : This helps to run script in debug mode
       
       
 #### Execution  
 
 - Enter the command in below format  
      - --{argument as per requirement (See main.py}> --vv
      
 - All scripts gives output as a file.
 - Logs will be stored in reservation.log file.
 
 - Passwords for db connection are to be given in config.py.
 
         
#### usage

usage: main.py [-h] [--read_populate] [--update_db] [--unapproved_list]
               [--ec2_reserve] [--overdue_instances] [--savings_plan] [-vv]

Command line interface to populate db, update db, get unapproved instances
list, normalization factor

optional arguments:
  -h, --help          show this help message and exit  
  --read_populate      read google spreadsheet data and populate data to db.  
                      This can be used for first time a new sheet created
  --update_db          Update db for any updates in google spreadsheet  
  --unapproved_list    Get list of unapproved instances  
  --ec2_reserve          Get normalization factor  
  --overdue_instances  Get normalization factor  
  --savings_plan       Get recommendations could help you reduce your costs  
  -vv                 Increase Logging verbosity  



