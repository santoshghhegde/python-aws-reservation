import argparse
import sys
import os
import pandas as pd
import read_gspread
import update_db
import unapproved_instances
import reservation
import end_date_calculation_notification as overdue_instance
import dry_run
import get_base_values 

def cli_args():
    """
    This function sets up the command line arguments
    :return:
    """

    parser = argparse.ArgumentParser(description="Command line interface to populate db, update db,\
                                                 get unapproved instances list, normalization factor")
    parser.add_argument("--read_populate",help="read google spreadsheet data and populate data to db. This can be\
                                  used for first time a new sheet created",action="store_true", default=False)
    parser.add_argument("--update-db",help="Update db for any updates in google spreadsheet",action="store_true",
                        default=False)
    parser.add_argument("--unapproved_list", help="Get list of unapproved instances", action="store_true",
                        default=False)
    parser.add_argument("--ec2_reserve", help="Get list of ec2 intances to reserve", action="store_true",
                        default=False)
    parser.add_argument("--overdue_instances", help="Get Overdue instance list", action="store_true",
                        default=False)
    parser.add_argument("--savings_plan", help="Get recommendations could help you reduce your costs", action="store_true",
                        default=False)
    parser.add_argument("--dry_run", help="Dry run to know what gets updated in db before actually updating it",
                        action="store_true", default=False)

    parser.add_argument("--get_ec2_base_values", help="get the base values of all ec2 instances available for a particular region", action="store_true")

    parser.add_argument("--get_rds_reservation", help="print table of overutilized and underutilized rds resources", action="store_true")
    parser.add_argument("--get_redshift_reservation", help="print table of overutilized and underutilized redshift resources", action="store_true")
    parser.add_argument("--get_elasticache_reservation", help="print table of overutilized and underutilized elasticache resources", action="store_true")


    return parser.parse_args()

def parse_args(args):

    if args.read_populate:
        read_gspread.create_table_and_populate_data()

    elif args.update_db:
        update_db.update_db()

    elif args.unapproved_list:
        unapproved_instances.get_unapproved_instances_list()

    elif args.ec2_reserve:
        reservation.ec2()

    elif args.overdue_instances:
        overdue_instance.get_due_instances()

    elif args.dry_run:
        dry_run.dry_run()
    
    elif args.get_ec2_base_values:
        get_base_values.get_base()

    elif args.get_rds_reservation:
        combined = normalize.rds_normalized()
        if(isinstance(combined, pd.DataFrame) and not combined.empty):
            print("\n RDS:")
            print(combined)
            #under.to_csv("underutilized_rds.csv",index=False)
        
    elif args.get_redshift_reservation:
        combined = normalize.redshift_normalized()
        if(isinstance(combined, pd.DataFrame) and not combined.empty):
            print("\n Redshift:")
            print(combined)
            #under.to_csv("underutilized_rds.csv",index=False)

    elif args.get_elasticache_reservation:
        combined = normalize.elasticache_normalize()
        if(isinstance(combined, pd.DataFrame) and not combined.empty):
            print("\n Elasticache:")
            print(combined)
            #under.to_csv("underutilized_rds.csv",index=False)
    else:
        print("Wrong command line inputs. Please check the command line arguments!!")
        sys.exit(1)



if __name__ == '__main__':

    args = cli_args()
    parse_args(args)